# Meetups Server on Flask

Сайт: https://meetups-flask.shgk.me

**Логин/Пароль для тестирования**: me@shgk.me / qwerty

API Docs: https://meetups-flask.shgk.me/api

## Установка

1. Установить python3.7+
1. `pip install -r requirements.txt`
1. Скопируйте файл `.env.example` и сохраните под именем `.env`
1. Заполните переменные окружения `.env`, если необходимо

## Запуск

#### Разработка
`python -m meetups_server`

#### Продакшн
`gunicorn meetups_server.__main__:app`

## База данных

![DB Schema](./screenshots/database_schema.png)

## Альтернативный вариант - Dockerfile
