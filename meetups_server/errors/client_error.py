class ClientError(Exception):
    def to_dict(self):
        return {
            'error': str(self),
            'message': str(self),
        }
