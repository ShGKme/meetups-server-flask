import os
from datetime import timedelta
from dotenv import load_dotenv
from flask import Flask, make_response, request, abort, jsonify, render_template
from flask_cors import CORS
from flask_login import LoginManager, login_user, login_required, logout_user, current_user
from flask_swagger_ui import get_swaggerui_blueprint
from humps import camelize

from meetups_server.data import db_session
from meetups_server.errors.client_error import ClientError
from meetups_server.seeder import seed_if_require
from meetups_server.services.auth_service import register_user, verify_user
from meetups_server.services.meetups_service import find_all_meetups, find_one_meetup, attend_meetup, leave_meetup, \
    create_meetup, remove_meetup
from meetups_server.services.users_service import find_user_by_id

load_dotenv()


def create_app():
    app = Flask(__name__, static_folder='./static')
    CORS(app)

    app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY', 'secret_key')
    app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=30)

    db_session.global_init(os.environ.get('DATABASE_URI', f'sqlite:///db.sqlite3?check_same_thread=False'))
    seed_if_require()

    return app


app = create_app()

login_manager = LoginManager()
login_manager.init_app(app)

swagger_blueprint = get_swaggerui_blueprint('/api', '/static/swagger.json')
app.register_blueprint(swagger_blueprint, url_prefix='/api')


@login_manager.user_loader
def load_user(user_id):
    return find_user_by_id(user_id)


@app.route('/api/ping')
def ping():
    return 'pong'


@app.route('/api/auth/register', methods=['POST'])
def register():
    if not request.json:
        return abort(400)
    error, user = register_user(request.json)
    if error:
        return jsonify(error.to_dict())
    return jsonify(user.to_dict())


@app.route('/api/auth/login', methods=['POST'])
def login():
    if not request.json:
        abort(400)
    error, user = verify_user(request.json)
    if error:
        return jsonify(error.to_dict())
    login_user(user, remember=True)
    return jsonify(user.to_dict())


@app.route('/api/auth/logout', methods=['POST'])
@login_required
def logout():
    logout_user()
    return '', 204


@app.route('/api/auth/user', methods=['GET'])
@login_required
def get_current_user():
    return jsonify(current_user.to_dict())


@app.route('/api/meetups')
def get_meetups():
    meetups_dicts = []
    for meetup in find_all_meetups(current_user):
        meetup_dict = meetup.to_dict()
        meetup_dict['organizer'] = meetup.organizer.fullname
        meetups_dicts.append(meetup_dict)
    return jsonify(camelize(meetups_dicts))


@app.route('/api/meetups/<int:meetup_id>')
def get_meetup(meetup_id):
    meetup = find_one_meetup(meetup_id, current_user)
    if not meetup:
        return abort(404)
    meetup_dict = meetup.to_dict(rules=('agenda',))
    meetup_dict['organizer'] = meetup.organizer.fullname
    return jsonify(camelize(meetup_dict))


@app.route('/api/meetups/<int:meetup_id>/participation', methods=['PUT'])
@login_required
def put_meetup_participation(meetup_id):
    meetup = attend_meetup(meetup_id, current_user)
    if not meetup:
        return abort(404)
    return '', 204


@app.route('/api/meetups/<int:meetup_id>/participation', methods=['DELETE'])
@login_required
def delete_meetup_participation(meetup_id):
    meetup = leave_meetup(meetup_id, current_user)
    if not meetup:
        return abort(404)
    return '', 204


@app.route('/api/meetups', methods=['POST'])
@login_required
def post_meetup():
    if not request.json:
        return abort(400)
    meetup = create_meetup(request.json, current_user)
    meetup_dict = meetup.to_dict(rules=('agenda',))
    meetup_dict['organizer'] = meetup.organizer.fullname
    return jsonify(camelize(meetup_dict))


@app.route('/api/meetups/<int:meetup_id>', methods=['PUT'])
@login_required
def put_meetup(meetup_id):
    return jsonify(ClientError('Not Implemented (´。＿。｀)').to_dict()), 501


@app.route('/api/meetups/<int:meetup_id>', methods=['DELETE'])
@login_required
def delete_meetup(meetup_id):
    error = remove_meetup(meetup_id, current_user)
    if error:
        return jsonify(error.to_dict())
    return '', 204


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return app.send_static_file('index.html')


@app.errorhandler(404)
def not_found(error):
    return make_response('Not found', 404)


def run():
    app.run(
        host=os.environ.get('HOST', '0.0.0.0'),
        port=int(os.environ.get('PORT', '4000'))
    )


__all__ = ['run', 'app']
