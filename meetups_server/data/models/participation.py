from sqlalchemy import Table, Column, Integer, ForeignKey

from meetups_server.data.db_session import SqlAlchemyBase

Participation = Table('participation', SqlAlchemyBase.metadata,
                      Column('meetup_id', Integer, ForeignKey('meetups.id')),
                      Column('user_id', Integer, ForeignKey('users.id'))
                      )
