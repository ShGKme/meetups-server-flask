from sqlalchemy import orm, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy_serializer import SerializerMixin
from ..db_session import SqlAlchemyBase


class AgendaItem(SqlAlchemyBase, SerializerMixin):
    __tablename__ = 'agenda_items'

    id = Column(Integer, primary_key=True, autoincrement=True)
    meetup_id = Column(Integer, ForeignKey("meetups.id"))
    starts_at = Column(String)
    ends_at = Column(String)
    type = Column(String)
    title = Column(String, nullable=True)
    description = Column(String, nullable=True)
    speaker = Column(String, nullable=True)
    language = Column(String, nullable=True)

    meetup = orm.relation('Meetup')

    serialize_rules = ('-meetup',)
