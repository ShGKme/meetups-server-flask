from sqlalchemy import orm, Column, Integer, String, DateTime, ForeignKey, Table
from sqlalchemy_serializer import SerializerMixin

from ..db_session import SqlAlchemyBase


class Meetup(SqlAlchemyBase, SerializerMixin):
    __tablename__ = 'meetups'

    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String)
    description = Column(String, nullable=True)
    place = Column(String, nullable=True)
    cover = Column(String, nullable=True)
    date = Column(DateTime)
    organizer_id = Column(Integer, ForeignKey("users.id"))

    organizer = orm.relation("User", foreign_keys=[organizer_id])
    agenda = orm.relation("AgendaItem", back_populates='meetup')
    participants = orm.relation("User", secondary='participation', backref='attending_meetups')

    organizing = None
    attending = None

    date_format = '%s'
    serialize_only = ('id', 'title', 'description', 'date', 'place', 'cover', 'organizing', 'attending')

