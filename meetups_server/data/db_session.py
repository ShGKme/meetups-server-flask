import sqlalchemy
from sqlalchemy import orm
import sqlalchemy.ext.declarative as dec

SqlAlchemyBase = dec.declarative_base()

__factory = None


def global_init(db_uri):
    global __factory

    if __factory:
        return

    print(f'Подключение к базе данных по адресу {db_uri}')

    engine = sqlalchemy.create_engine(db_uri, echo=False)
    __factory = orm.sessionmaker(bind=engine)

    from . import models
    SqlAlchemyBase.metadata.create_all(engine)


def create_session() -> orm.Session:
    global __factory
    return __factory()
