from datetime import datetime

from meetups_server.data import db_session
from meetups_server.data.models import User, Meetup, AgendaItem


def seed_if_require():
    db = db_session.create_session()
    users = db.query(User).all()
    if not users:
        seed()


def seed():
    db = db_session.create_session()

    user_me = User(
        email='me@shgk.me',
        fullname='Grigorii K. Shartsev',
    )
    user_me.set_password('qwerty')

    user_igor_sh = User(
        email='igor@email',
        fullname='Игорь Ш.',
    )
    user_igor_sh.set_password('qwerty')

    user_eugene_f = User(
        email='eugeny@email',
        fullname='Eugeny F.',
    )
    user_eugene_f.set_password('qwerty')

    msk_vue_js_meetup1 = Meetup(
        title='MSK VUE.JS MEETUP #1',
        description='С каждым днем Vue.js становится популярней, все больше разработчиков и компаний делают ставку на данную технологию — 18 июля при поддержке компании Voximplant пройдет митап сообщества MSK VUE.JS, посвященный фреймворку. Спикеры поделятся опытом разработки, участники сообщества обсудят перспективы развития Vue.js.',
        date=datetime(2019, 7, 18),
        place='Москва, офис Voximplant (ул. Мытная 66)',
        cover='https://secure.meetupstatic.com/photos/event/7/4/2/600_468241858.jpeg',
    )
    msk_vue_js_meetup1.organizer = user_igor_sh

    msk_vue_js_meetup1.agenda = [
        AgendaItem(
            starts_at='18:30',
            ends_at='19:00',
            type='registration',
        ),
        AgendaItem(
            starts_at='19:00',
            ends_at='19:45',
            type='talk',
            title='Vue.js 3 — все что ждет нас в будущем',
            description='Скоро нас ждет Vue.js 3. Теперь наш любимый фреймворк станет лучше, быстрее, моднее. Давайте поговорим, что нового нас ждет, что мы получим и что потеряем в результате обновления. Рассмотрим технологии, которыми обогатится Vue.js и которые сделают его, на мой взгляд, самым быстрым и простым фреймворком на рынке.',
            speaker='Игорь Шеко — Lead Developer, Voximplant',
        ),
        AgendaItem(
            starts_at='19:45',
            ends_at='20:15',
            type='coffee',
        ),
        AgendaItem(
            starts_at='20:15',
            ends_at='21:00',
            type='talk',
            title='Опыт использования Vue.js в «Едадиле»',
            description='— Долгая жизнь с Vue: промышленное использования начиная с версии 0.x.\n' +
                        '— Vue внутри webview нативного приложения: подводные камни.\n' +
                        '— Не «стандартный подход» к организации кода и сборке.\n' +
                        '— Авто-оптимизация приложения Vue.\n' +
                        '— Настоящая изоморфность: Vue и другой рантайм.',
            speaker='Андрей Кобец — Руководитель отдела разработки фронтенда «Едадила», Яндекс',
        ),
        AgendaItem(
            starts_at='21:00',
            ends_at='21:45',
            type='talk',
            title='Прогрессивные приложения на прогрессивном фреймворке',
            description='PWA (progressive web app, прогрессивное веб-приложение) — один из главных трендов в вебе последние 2 года. Наверняка, вы не раз о нем слышали и даже не раз делали. Или, как я, давно хотели попробовать, но на рабочих проектах в нем не было необходимости. Этот доклад для неслышавших или слышавших, но не пробовавших. Расскажу, что такое PWA, какие технологии подразумевает, в каких браузерах они работают, как интегрировать их во vue-приложение, чем тестировать и какие удобные готовые решения есть можно использовать.',
            speaker='Ольга Лесникова — Senior Developer, Voximplant',
        ),
        AgendaItem(
            starts_at='22:00',
            ends_at='22:00',
            type='closing',
        ),
    ]

    msk_vue_js_meetup2 = Meetup(
        title='Мой митап',
        description='',
        date=datetime(2019, 7, 19),
        place='Пермь',
    )
    msk_vue_js_meetup2.organizer = user_me

    db.add_all([user_me, user_igor_sh, user_eugene_f])
    db.add_all([msk_vue_js_meetup1, msk_vue_js_meetup2])
    db.commit()
