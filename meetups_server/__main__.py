from meetups_server.server import run, app

if __name__ == '__main__':
    run()

__all__ = ['run', 'app']
