from meetups_server.data import db_session
from meetups_server.data.models import User


def find_user_by_id(user_id):
    db = db_session.create_session()
    user = db.query(User).get(user_id)
    db.close()
    return user


def find_user_by_email(email):
    db = db_session.create_session()
    user = db.query(User).filter(User.email == email).first()
    db.close()
    return user
