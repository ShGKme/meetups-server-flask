from meetups_server.data import db_session
from meetups_server.data.models import User
from meetups_server.errors.client_error import ClientError
from meetups_server.services.users_service import find_user_by_email


def register_user(register_dto) -> (ClientError, User):
    db = db_session.create_session()

    if db.query(User).filter(User.email == register_dto['email']).first():
        return ClientError('Пользователь с таким Email уже зарегистрирован'), None

    user = User(
        fullname=register_dto['fullname'],
        email=register_dto['email']
    )
    user.set_password(register_dto['password'])
    db.add(user)
    db.commit()

    return None, user


def verify_user(login_dto: dict) -> (ClientError, User):
    user = find_user_by_email(login_dto['email'])
    error_message = 'Неверные учетные данные'
    if not user or not user.check_password(login_dto['password']):
        return ClientError(error_message), None
    return None, user
