from datetime import datetime
from typing import List

from meetups_server.data import db_session
from meetups_server.data.models import Meetup, AgendaItem, User
from meetups_server.errors.client_error import ClientError


def find_all_meetups(user: User = None) -> List[Meetup]:
    db = db_session.create_session()
    meetups = db.query(Meetup).all()
    meetups = [_take_user_in_meetup(meetup, user) for meetup in meetups]
    return meetups


def find_one_meetup(meetup_id: int, user: User) -> Meetup:
    db = db_session.create_session()
    meetup = db.query(Meetup).get(meetup_id)
    meetup = _take_user_in_meetup(meetup, user)
    return meetup


def _take_user_in_meetup(meetup: Meetup, user: User) -> Meetup:
    if not meetup or not user:
        return meetup
    meetup.organizing = meetup.organizer == user
    meetup.attending = user in meetup.participants
    return meetup


def attend_meetup(meetup_id: int, user: User):
    db = db_session.create_session()
    db.merge(user)
    meetup: Meetup = db.query(Meetup).get(meetup_id)
    if not meetup:
        return False
    meetup.participants.append(user)
    db.commit()
    return True


def leave_meetup(meetup_id: int, user: User) -> bool:
    db = db_session.create_session()
    db.merge(user)
    meetup: Meetup = db.query(Meetup).get(meetup_id)
    if not meetup:
        return False
    meetup.participants.remove(user)
    db.commit()
    return True


def create_meetup(meetup_dto: dict, organizer: User) -> Meetup:
    db = db_session.create_session()
    meetup = Meetup(
        title=meetup_dto.get('title', None),
        description=meetup_dto.get('description', None),
        date=datetime.fromisoformat(meetup_dto['date']) if meetup_dto['date'] else datetime.today(),
        place=meetup_dto.get('place', None),
    )
    meetup.organizer = organizer
    for agenda_item_dto in meetup_dto['agenda']:
        meetup.agenda.append(AgendaItem(
            starts_at=agenda_item_dto.get('startsAt', None),
            ends_at=agenda_item_dto.get('endsAt', None),
            type=agenda_item_dto.get('type', None),
            title=agenda_item_dto.get('title', None),
            description=agenda_item_dto.get('description', None),
            speaker=agenda_item_dto.get('speaker', None),
        ))
    db.add(meetup)
    meetup.organizing = True
    meetup.attending = False
    db.commit()
    return meetup


def remove_meetup(meetup_id, user):
    db = db_session.create_session()
    db.merge(user)
    meetup: Meetup = db.query(Meetup).get(meetup_id)
    if not meetup:
        return ClientError('Митап не найден')
    if meetup.organizer != user:
        return ClientError('Forbidden')
    db.delete(meetup)
    db.commit()
